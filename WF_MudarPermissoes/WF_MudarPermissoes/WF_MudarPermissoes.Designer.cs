﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Workflow.Activities;
using System.Workflow.ComponentModel;
using System.Workflow.Runtime;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WorkflowActions;
using Utilities;

namespace WF_MudarPermissoes.Workflow1
{
    // Token: 0x02000002 RID: 2
    public sealed class Workflow1 : SequentialWorkflowActivity
    {
        // Token: 0x06000001 RID: 1 RVA: 0x000020D0 File Offset: 0x000002D0
        public Workflow1()
        {
            this.InitializeComponent();
        }

        // Token: 0x06000002 RID: 2 RVA: 0x000020FC File Offset: 0x000002FC
        private void OnWorkflowActivated(object sender, ExternalDataEventArgs e)
        {
            this.list = this.workflowProperties.List;
            this.user = this.workflowProperties.OriginatorUser;
            this.site = this.workflowProperties.Site;
            this.item = this.workflowProperties.Item;
            this.web = this.workflowProperties.Web;
            this.id = this.workflowProperties.Item.ID.ToString();
            SPRoleType sproleType = (SPRoleType)2;
            SPRoleType sproleType2 = (SPRoleType)3;
            SPRoleType sproleType3 = (SPRoleType)5;
            SPRoleDefinition byType = this.workflowProperties.Web.RoleDefinitions.GetByType(sproleType);
            SPRoleDefinition byType2 = this.workflowProperties.Web.RoleDefinitions.GetByType(sproleType2);
            SPRoleDefinition byType3 = this.workflowProperties.Web.RoleDefinitions.GetByType(sproleType3);
            try
            {
                UtilitiesSP2010v1 utilitiesSP2010v = new UtilitiesSP2010v1(base.Name);
                utilitiesSP2010v.DisableEventFiring();
                try
                {
                    utilitiesSP2010v.ClearPermissions(true, this.item, this.web, this.site);
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", "Limpar as permissões do item", ex.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                string text = "";
                string text2 = "";
                string text3 = "";
                string text4 = "";
                try
                {
                    text = utilitiesSP2010v.GetSPUserCollectionString("Destinatário", this.item, this.web);
                    text2 = utilitiesSP2010v.GetSPUserCollectionString("Unidade Orgânica", this.item, this.web);
                    text3 = utilitiesSP2010v.GetSPUserCollectionString("CC", this.item, this.web);
                    text4 = utilitiesSP2010v.GetSPUserCollectionString("EnviarPara", this.item, this.web);
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", "Obter users de um campo da lista", ex.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                try
                {
                    if (text != "")
                    {
                        utilitiesSP2010v.UsersPermissions(text, byType2, this.item, this.web);
                    }
                    if (text2 != "")
                    {
                        utilitiesSP2010v.UsersPermissions(text2, byType2, this.item, this.web);
                    }
                    if (text3 != "")
                    {
                        utilitiesSP2010v.UsersPermissions(text3, byType, this.item, this.web);
                    }
                    if (text4 != "")
                    {
                        utilitiesSP2010v.UsersPermissions(text4, byType, this.item, this.web);
                    }
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", "Definir permissões conforme a origem", ex.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                string text5 = "";
                string text6 = "";
                try
                {
                    text5 = this.GetItemPermissionsParameters("Reader");
                    text6 = this.GetItemPermissionsParameters("Contributor");
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", "Obter users de uma lista de parâmetros", ex.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                try
                {
                    if (text5 != "")
                    {
                        utilitiesSP2010v.UsersPermissions(text5, byType, this.item, this.web);
                    }
                    if (text6 != "")
                    {
                        utilitiesSP2010v.UsersPermissions(text6, byType2, this.item, this.web);
                    }
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", "Definir permissões conforme a origem", ex.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                string text7 = "";
                string text8 = "";
                int num = 0;
                try
                {
                    num++;
                    text8 = this.item["Editor"].ToString();
                    num++;
                    string text9 = this.item["Informa_x00e7__x00e3_o"].ToString();
                    num++;
                    text7 = ((this.item["BodyMail"] != null) ? this.item["BodyMail"].ToString() : "");
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1} [{2}]", "Obter campos do item", ex.ToString(), num.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                try
                {
                    if (text4 != "")
                    {
                        this.item["BodyMail"] = "";
                    }
                    this.item.Update();
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", "Actualizar o item", ex.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                try
                {
                    if (text4 != "")
                    {
                        text8 = text8.Substring(text8.LastIndexOf("#") + 1).TrimEnd(new char[0]);
                        text7 = "Reencaminhado por: " + text8 + "<br><hr><br>" + text7;
                        this.SendMailToNotify("intranet@utad.pt", text4, text7, this.id);
                    }
                    else
                    {
                        this.SendMailToNotify("intranet@utad.pt", this.ValidUsersToNotify(text + "; " + text3), "", this.id);
                    }
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", "Enviar e-mail aos utilizadores", ex.ToString()), EventLogEntryType.Error, 1232);
                    throw new Exception(string.Format("{0}", ex.ToString()));
                }
                utilitiesSP2010v.EnableEventFiring();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("OnWorkflowActivated: {0}:\n{1}", base.Name, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
        }

        // Token: 0x06000003 RID: 3 RVA: 0x00002868 File Offset: 0x00000A68
        private string ValidUsersToNotify(string users)
        {
            UtilitiesSP2010v1 utilitiesSP2010v = new UtilitiesSP2010v1(base.Name);
            string text = "";
            try
            {
                using (SPSite spsite = new SPSite("http://www.intra.utad.pt/sites/applications"))
                {
                    using (SPWeb spweb = spsite.OpenWeb())
                    {
                        ArrayList arrayList = new ArrayList(users.Split(new char[]
						{
							';'
						}));
                        SPList splist = spweb.Lists["Lista Distribuição"];
                        SPListItemCollection items = splist.Items;
                        foreach (object obj in items)
                        {
                            SPListItem spListItem = (SPListItem)obj;
                            string loginName = utilitiesSP2010v.GetSPUserObject(spListItem, "Utilizador").LoginName;
                            for (int i = 0; i < arrayList.Count; i++)
                            {
                                string text2 = arrayList[i] as string;
                                if (text2.Contains(loginName))
                                {
                                    arrayList.RemoveAt(i);
                                }
                            }
                        }
                        foreach (object arg in arrayList)
                        {
                            text = text + ((text != "") ? "; " : "") + arg;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("ValidUsersToNotify: {0}:\n{1}", base.Name, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
            return text;
        }

        // Token: 0x06000004 RID: 4 RVA: 0x00002AD8 File Offset: 0x00000CD8
        private void SendMailToNotify(string from, string users, string body, string id)
        {
            string text = this.item.Url.Substring(0, this.item.Url.LastIndexOf("/"));
            string arg = (this.item["Informacao"] != null) ? ("[Inf.: " + this.item["Informacao"].ToString() + "]") : "";
            string arg2 = "";
            Regex regex = new Regex("(\\d{4})");
            Match match = regex.Match(text);
            if (match.Success)
            {
                arg2 = match.Groups[1].Value;
            }
            string text2 = string.Format("{0}/paginas/gestaoExpedienteGeralConsulta.aspx?ID='{1}'&Year='{2}'", this.web.Url, id, arg2);
            text2 = text2.Replace("Lists/Documentos Digitalizados", "paginas");
            string text3 = string.Format("{0}/{1}/DispForm.aspx?ID={2}&PageView=Shared&InitialTabId=Ribbon.Read&VisibilityContext=WSSWebPartPage", this.web.Url, text, id);
            if (body == "")
            {
                string text4 = body;
                body = string.Concat(new string[]
				{
					text4,
					"Encontra-se para consulta um documento digitalizado.<br>Documento: <a href=\"",
					text2,
					"\">",
					this.item.Name,
					"</a><br><small><a href=\"",
					text3,
					"\">Abrir documento em formato HTML</a></small><br><hr><small>Nota: Este email foi gerado automaticamente.</small>"
				});
            }
            else
            {
                string text4 = body;
                body = string.Concat(new string[]
				{
					text4,
					"<br><br><hr>Encontra-se para consulta um documento digitalizado.<br>Documento: <a href=\"",
					text2,
					"\">",
					this.item.Name,
					"</a><br><small><a href=\"",
					text3,
					"\">Abrir documento em formato HTML</a></small><br><hr><small>Nota: Este email foi gerado automaticamente.</small>"
				});
            }
            string[] array = users.Split(new char[]
			{
				';'
			});
            foreach (string text5 in array)
            {
                string text6 = text5.Substring(text5.LastIndexOf("\\") + 1).TrimEnd(new char[0]);
                if (text6 != "")
                {
                    StringDictionary stringDictionary = new StringDictionary();
                    stringDictionary.Add("to", text6 + "@utad.pt");
                    stringDictionary.Add("cc", "");
                    stringDictionary.Add("bcc", "");
                    stringDictionary.Add("from", from);
                    stringDictionary.Add("subject", string.Format("Intranet: Documento digitalizado para consulta. {0}", arg));
                    stringDictionary.Add("content-type", "text/html");
                    //bool flag = SPUtility.SendEmail(this.web, stringDictionary, body);
                }
            }
        }

        // Token: 0x06000005 RID: 5 RVA: 0x00002DAC File Offset: 0x00000FAC
        private string GetItemPermissionsParameters(string role)
        {
            UtilitiesSP2010v1 utilitiesSP2010v = new UtilitiesSP2010v1(base.Name);
            string result = "";
            try
            {
                SPList splist = this.web.Lists["Parâmetros Expediente"];
                SPListItemCollection items = splist.Items;
                SPListItemCollection items2 = splist.GetItems(new SPQuery
                {
                    Query = "<Where><Eq><FieldRef Name='Permissoes' /><Value Type='Text'>" + role + "</Value></Eq></Where>",
                    ViewAttributes = "Scope=\"RecursiveAll\""
                });
                if (items2 != null && items2.Count > 0)
                {
                    SPListItem splistItem = items2[0];
                    SPListItem itemById = splistItem.ParentList.GetItemById(splistItem.ID);
                    result = utilitiesSP2010v.GetSPUserCollectionString("Grupo", itemById, this.web);
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("GetItemPermissionsParameters: {0}:\n{1}", base.Name, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
            return result;
        }

        // Token: 0x06000006 RID: 6 RVA: 0x00002ECC File Offset: 0x000010CC
        [DebuggerNonUserCode]
        private void InitializeComponent()
        {
            base.CanModifyActivities = true;
            ActivityBind activityBind = new ActivityBind();
            CorrelationToken correlationToken = new CorrelationToken();
            ActivityBind activityBind2 = new ActivityBind();
            this.onWorkflowActivated1 = new OnWorkflowActivated();
            activityBind.Name = "Workflow1";
            activityBind.Path = "workflowId";
            correlationToken.Name = "workflowToken";
            correlationToken.OwnerActivityName = "Workflow1";
            this.onWorkflowActivated1.CorrelationToken = correlationToken;
            this.onWorkflowActivated1.EventName = "OnWorkflowActivated";
            this.onWorkflowActivated1.Name = "onWorkflowActivated1";
            activityBind2.Name = "Workflow1";
            activityBind2.Path = "workflowProperties";
            this.onWorkflowActivated1.Invoked += this.OnWorkflowActivated;
            this.onWorkflowActivated1.SetBinding(Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated.WorkflowIdProperty, activityBind);
            this.onWorkflowActivated1.SetBinding(Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated.WorkflowPropertiesProperty, activityBind2);
            base.Activities.Add(this.onWorkflowActivated1);
            base.Name = "Workflow1";
            base.CanModifyActivities = false;
        }

        // Token: 0x04000001 RID: 1
        public Guid workflowId = default(Guid);

        // Token: 0x04000002 RID: 2
        public SPWorkflowActivationProperties workflowProperties = new SPWorkflowActivationProperties();

        // Token: 0x04000003 RID: 3
        private SPList list;

        // Token: 0x04000004 RID: 4
        private SPUser user;

        // Token: 0x04000005 RID: 5
        private SPSite site;

        // Token: 0x04000006 RID: 6
        private SPWeb web;

        // Token: 0x04000007 RID: 7
        private SPListItem item;

        // Token: 0x04000008 RID: 8
        private string id;

        // Token: 0x04000009 RID: 9
        private OnWorkflowActivated onWorkflowActivated1;
    }
}
