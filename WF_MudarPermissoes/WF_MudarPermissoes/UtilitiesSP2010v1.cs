﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using Microsoft.SharePoint;

namespace Utilities
{
    // Token: 0x02000003 RID: 3
    internal class UtilitiesSP2010v1 : SPItemEventReceiver
    {
        // Token: 0x17000001 RID: 1
        // (get) Token: 0x06000007 RID: 7 RVA: 0x00002FD8 File Offset: 0x000011D8
        // (set) Token: 0x06000008 RID: 8 RVA: 0x00002FF0 File Offset: 0x000011F0
        public string ProjectName
        {
            get
            {
                return this.projectName;
            }
            set
            {
                this.projectName = value;
            }
        }

        // Token: 0x06000009 RID: 9 RVA: 0x00002FFA File Offset: 0x000011FA
        public UtilitiesSP2010v1()
        {
        }

        // Token: 0x0600000A RID: 10 RVA: 0x00003005 File Offset: 0x00001205
        public UtilitiesSP2010v1(string prjName)
        {
            this.projectName = prjName;
        }

        // Token: 0x0600000B RID: 11 RVA: 0x00003018 File Offset: 0x00001218
        public void DisableEventFiring()
        {
            try
            {
                base.EventFiringEnabled = false;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("GetSPUserCollectionString: {0}:\n{1}", "DisableEventFiring", ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
        }

        // Token: 0x0600000C RID: 12 RVA: 0x00003084 File Offset: 0x00001284
        public void EnableEventFiring()
        {
            try
            {
                base.EventFiringEnabled = true;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("GetSPUserCollectionString: {0}:\n{1}", "EnableEventFiring", ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
        }

        // Token: 0x0600000D RID: 13 RVA: 0x000030F0 File Offset: 0x000012F0
        public string GetSPUserCollectionString(string fieldName, SPListItem spListItem, SPWeb web)
        {
            string text = "";
            try
            {
                if (fieldName != string.Empty)
                {
                    SPFieldUser spfieldUser = spListItem.Fields[fieldName] as SPFieldUser;
                    if (spfieldUser != null && spListItem[fieldName] != null)
                    {
                        SPFieldUserValueCollection spfieldUserValueCollection = spfieldUser.GetFieldValue(spListItem[fieldName].ToString()) as SPFieldUserValueCollection;
                        if (spfieldUserValueCollection != null)
                        {
                            foreach (SPFieldUserValue spfieldUserValue in spfieldUserValueCollection)
                            {
                                if (spfieldUserValue.User != null)
                                {
                                    SPUser user = spfieldUserValue.User;
                                    text = text + ((text != "") ? "; " : "") + user.LoginName;
                                }
                                else
                                {
                                    SPGroup byID = web.Groups.GetByID(spfieldUserValue.LookupId);
                                    text = text + ((text != "") ? "; " : "") + (byID.Name.StartsWith("intra") ? byID.Name : ("intra\\" + byID.Name));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("GetSPUserCollectionString: {0}:\n{1}", this.ProjectName, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
            return text;
        }

        // Token: 0x0600000E RID: 14 RVA: 0x000032D4 File Offset: 0x000014D4
        public SPUserCollection GetSPUserCollectionObject(SPListItem spListItem, string fieldName)
        {
            SPUserCollection spuserCollection = null;
            try
            {
                if (fieldName != string.Empty)
                {
                    SPFieldUser spfieldUser = spListItem.Fields[fieldName] as SPFieldUser;
                    if (spfieldUser != null && spListItem[fieldName] != null)
                    {
                        SPFieldUserValueCollection spfieldUserValueCollection = spfieldUser.GetFieldValue(spListItem[fieldName].ToString()) as SPFieldUserValueCollection;
                        if (spfieldUserValueCollection != null)
                        {
                            foreach (SPFieldUserValue spfieldUserValue in spfieldUserValueCollection)
                            {
                                SPUser user = spfieldUserValue.User;
                                spuserCollection.Add(user.LoginName, user.Email, user.Name, user.Notes);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("GetSPUserCollectionObject: {0}:\n{1}", this.ProjectName, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
            return spuserCollection;
        }

        // Token: 0x0600000F RID: 15 RVA: 0x00003418 File Offset: 0x00001618
        public SPUser GetSPUserObject(SPListItem spListItem, string fieldName)
        {
            SPUser result = null;
            try
            {
                if (fieldName != string.Empty)
                {
                    SPFieldUser spfieldUser = spListItem.Fields[fieldName] as SPFieldUser;
                    if (spfieldUser != null && spListItem[fieldName] != null)
                    {
                        SPFieldUserValue spfieldUserValue = spfieldUser.GetFieldValue(spListItem[fieldName].ToString()) as SPFieldUserValue;
                        if (spfieldUserValue != null)
                        {
                            result = spfieldUserValue.User;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("GetSPUserObject: {0}:\n{1}", this.ProjectName, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
            return result;
        }

        // Token: 0x06000010 RID: 16 RVA: 0x000034F0 File Offset: 0x000016F0
        public string GetUserCollection(string users, SPWeb web)
        {
            string result = "";
            try
            {
                if (users != "")
                {
                    SPFieldUserValueCollection spfieldUserValueCollection = new SPFieldUserValueCollection();
                    string[] array = users.Split(new char[]
					{
						';'
					});
                    foreach (string text in array)
                    {
                        SPUser spuser = web.EnsureUser(text);
                        spfieldUserValueCollection.Add(new SPFieldUserValue(web, spuser.ID, spuser.Name));
                    }
                    result = spfieldUserValueCollection.ToString();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("GetUserCollection: {0}:\n{1}", this.ProjectName, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
            return result;
        }

        // Token: 0x06000011 RID: 17 RVA: 0x000035E8 File Offset: 0x000017E8
        public void UsersPermissions(string users, SPRoleDefinition role, SPListItem item, SPWeb web)
        {
            try
            {
                if (users != "")
                {
                    string[] array = users.Split(new char[]
					{
						';'
					});
                    foreach (string text in array)
                    {
                        SPUser spuser = web.EnsureUser(text);
                        SPRoleAssignment sproleAssignment = new SPRoleAssignment(spuser.LoginName, spuser.Email, spuser.Name, spuser.Notes);
                        sproleAssignment.RoleDefinitionBindings.Add(role);
                        item.RoleAssignments.Add(sproleAssignment);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("UsersPermissions: {0}:\n{1}", this.ProjectName, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
        }

        // Token: 0x06000012 RID: 18 RVA: 0x000036E0 File Offset: 0x000018E0
        public void ClearPermissions(bool clearPermissions, SPListItem itemSystem, SPWeb webSystem, SPSite siteSystem)
        {
            try
            {
                if (clearPermissions)
                {
                    if (!itemSystem.HasUniqueRoleAssignments)
                    {
                        itemSystem.BreakRoleInheritance(false);
                    }
                    webSystem.AllowUnsafeUpdates = true;
                    siteSystem.AllowUnsafeUpdates = true;
                    while (itemSystem.RoleAssignments.Count != 0)
                    {
                        itemSystem.RoleAssignments.Remove(itemSystem.RoleAssignments.Count - 1);
                    }
                }
                else
                {
                    if (!itemSystem.HasUniqueRoleAssignments)
                    {
                        itemSystem.BreakRoleInheritance(true);
                    }
                    webSystem.AllowUnsafeUpdates = true;
                    siteSystem.AllowUnsafeUpdates = true;
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Workflow", string.Format("ClearPermissions: {0}:\n{1}", this.ProjectName, ex.ToString()), EventLogEntryType.Error, 1232);
                throw new Exception(string.Format("{0}", ex.ToString()));
            }
        }

        // Token: 0x06000013 RID: 19 RVA: 0x000037C8 File Offset: 0x000019C8
        public string ConvertStringToUnicodeHex(string text)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char c in text)
            {
                if (c < '0' || c > 'z' || (c > '9' && c < 'A') || (c > 'Z' && c < 'a'))
                {
                    stringBuilder.Append(string.Format("_x{0:X4}_", (int)c));
                }
                else
                {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString();
        }

        // Token: 0x06000014 RID: 20 RVA: 0x00003860 File Offset: 0x00001A60
        public static string ConvertUnicodeHexToString(string inputString)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < inputString.Length; i += 7)
            {
                string s = inputString.Substring(i, 7).Substring(2, 4);
                char value = (char)uint.Parse(s, NumberStyles.HexNumber);
                stringBuilder.Append(value);
            }
            return stringBuilder.ToString();
        }

        // Token: 0x0400000A RID: 10
        private string projectName;
    }
}
